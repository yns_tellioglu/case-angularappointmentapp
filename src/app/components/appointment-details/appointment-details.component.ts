import { Component, OnInit } from '@angular/core';
import { AppointmentService } from 'src/app/services/appointment.service';
import { ActivatedRoute, Router } from '@angular/router';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-appointment-details',
  templateUrl: './appointment-details.component.html',
  styleUrls: ['./appointment-details.component.css']
})
export class AppointmentDetailsComponent implements OnInit {

  currentAppointmentEdit = null;
  message = '';
  model: NgbDateStruct;
  date: {year: number, month: number};

  constructor(
    private appointmentService: AppointmentService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.message = '';
    this.getProduct(this.route.snapshot.paramMap.get('id'));
  }

  getProduct(id): void {
    this.appointmentService.read(id)
      .subscribe(
        product => {
          this.currentAppointmentEdit = product;
          console.log(product);
        },
        error => {
          console.log(error);
        });
  }

  setAvailableStatus(status): void {
    const data = {
      userName: this.currentAppointmentEdit.name,
      Note: this.currentAppointmentEdit.description
    };

    this.appointmentService.update(this.currentAppointmentEdit.id, data)
      .subscribe(
        response => {
          console.log(response);
        },
        error => {
          console.log(error);
        });
  }

  updateProduct(): void {
    this.appointmentService.update(this.currentAppointmentEdit.id, this.currentAppointmentEdit)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'The product was updated!';
        },
        error => {
          console.log(error);
          this.message = 'The product not updated!';
        });
  }

  deleteProduct(): void {
    this.appointmentService.delete(this.currentAppointmentEdit.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/appointments']);
        },
        error => {
          console.log(error);
        });
  }

}
