import { Component, OnInit } from '@angular/core';
import { AppointmentService } from 'src/app/services/appointment.service';

@Component({
  selector: 'app-appointment-create',
  templateUrl: './appointment-create.component.html',
  styleUrls: ['./appointment-create.component.css']
})
export class AppointmentCreateComponent implements OnInit {

  appointment = {
    userId: '',
    userName: "",
    appointmentDate: "",
    appointmentTime: "",
    Note: ""
  };
  submitted = false;

  constructor(private appointmentService: AppointmentService) { }

  ngOnInit(): void {
  }

  createProduct(): void {
    const data = {
      userName: this.appointment.userName,
      Note: this.appointment.Note,
      appointmentDate: this.appointment.appointmentDate,
      appointmentTime: this.appointment.appointmentTime,
    };

    this.appointmentService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newProduct(): void {
    this.submitted = false;
    this.appointment = {
      userId : "",
      userName: "",
      Note: "",
      appointmentDate: "",
      appointmentTime: "",
    };
  }

}
