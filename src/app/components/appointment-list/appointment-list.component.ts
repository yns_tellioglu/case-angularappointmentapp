import { Component, OnInit } from '@angular/core';
import { AppointmentService } from 'src/app/services/appointment.service';

@Component({
  selector: 'app-appointment-list',
  templateUrl: './appointment-list.component.html',
  styleUrls: ['./appointment-list.component.css']
})
export class AppointmentListComponent implements OnInit {

  appointments: any;
  currentProduct = null;
  currentIndex = -1;
  name = '';

  constructor(private appointmentService: AppointmentService) { }

  ngOnInit(): void {
    this.readProducts();
  }

  readProducts(): void {
    this.appointmentService.readAll()
      .subscribe(
        appointments => {
          this.appointments = appointments;
          console.log(appointments);
        },
        error => {
          console.log(error);
        });
  }

  refresh(): void {
    this.readProducts();
    this.currentProduct = null;
    this.currentIndex = -1;
  }

  setCurrentProduct(appointments, index): void {
    this.currentProduct = appointments;
    this.currentIndex = index;
  }

  deleteAllProducts(): void {
    this.appointmentService.deleteAll()
      .subscribe(
        response => {
          console.log(response);
          this.readProducts();
        },
        error => {
          console.log(error);
        });
  }

  searchByName(): void {
    this.appointmentService.searchByName(this.name)
      .subscribe(
        products => {
          this.appointments = products;
          console.log(products);
        },
        error => {
          console.log(error);
        });
  }

}
