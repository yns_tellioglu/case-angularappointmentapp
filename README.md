# CaseAngularAppointmentApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.1.3.

Theme : Bootstrap , Mock Api : json-server 

## Install

Run the `npm install` command inside the project file.

`npm install --save json-server `

## Mock API server

Run `npm run server` for a dev server. Navigate to `http://localhost:3000/`. The app will automatically reload if you change any of the source files.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running 

* `npm install`
* `npm install --save json-server`
* `npm run server`
* `ng serve`

Please use two different terminals for npm run serve and ng serve


